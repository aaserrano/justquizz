package com.example.antonio.justquiz;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class LogoActivity extends AppCompatActivity {

    private String TAG ="LogoActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logo);
        MyLog.d(TAG,"Iniciando onCreate...");

        int secondsDelayed = 6;
        new Handler().postDelayed(new Runnable() {
            public void run() {
                startActivity(new Intent(LogoActivity.this, ResumenActivity.class));
                finish();
            }
        }, secondsDelayed * 1000);
    }

    @Override
    protected void onStart() {
        super.onStart();
        MyLog.d(TAG,"Iniciando onStart...");
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyLog.d(TAG,"Iniciando onResume...");
    }

    @Override
    protected void onPause() {
        super.onPause();
        MyLog.d(TAG,"Iniciando onPause...");
    }

    @Override
    protected void onStop() {
        super.onStop();
        MyLog.d(TAG,"Iniciando onStop...");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MyLog.d(TAG,"Iniciando onDestroy...");
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        MyLog.d(TAG,"Iniciando onRestart...");
    }
}
