package com.example.antonio.justquiz;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

public class CreaEditaActivity extends AppCompatActivity {

    String TAG = "CreaEditaActivity";

    private EditText enunciadopregunta;
    private EditText respuestacorrecta;
    private EditText respuestaincorrecta1;
    private EditText respuestaincorrecta2;
    private EditText respuestaincorrecta3;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crea_edita);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarcreaedita);
        setSupportActionBar(toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(getResources().getString(R.string.title_activity_creaedita));
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Asignar la acción necesaria. En este caso "volver atrás"
                    onBackPressed();
                }

            });


        }

        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        String[] categorias = {"Programación","Base de datos","Redes","Desarrollo de interfaces"};
        spinner.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categorias));


        EditText enunciadopregunta = (EditText)findViewById(R.id.editTextpregunta);
        EditText respuestacorrecta = (EditText)findViewById(R.id.editTextrespuestacorrecta);
        EditText respuestaincorrecta1 = (EditText)findViewById(R.id.editTextrespuestainco1);
        EditText respuestaincorrecta2 = (EditText)findViewById(R.id.editTextrespuestainco2);
        EditText respuestaincorrecta3 = (EditText)findViewById(R.id.editTextrespuestainco3);


    }





    @Override
    public void onBackPressed() {
        // Asignar la acción necesaria. En este caso terminar la actividad
        finish();
    }


    @Override
    protected void onResume() {
        super.onResume();
        MyLog.d(TAG,"Iniciando onResume...");
    }

    @Override
    protected void onPause() {
        super.onPause();
        MyLog.d(TAG,"Iniciando onPause...");
    }

    @Override
    protected void onStop() {
        super.onStop();
        MyLog.d(TAG,"Iniciando onStop...");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MyLog.d(TAG,"Iniciando onDestroy...");
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        MyLog.d(TAG,"Iniciando onRestart...");
    }


}
