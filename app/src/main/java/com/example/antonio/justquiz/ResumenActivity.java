package com.example.antonio.justquiz;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class ResumenActivity extends AppCompatActivity {


    private String TAG ="ResumenActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resumen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarresumen);
        setSupportActionBar(toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(getResources().getString(R.string.title_activity_resumen));
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Asignar la acción necesaria. En este caso "volver atrás"
                    onBackPressed();
                }

            });
            } else {
            MyLog.d("TAG", "Error al cargar toolbar");
        }

       }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menuresumen, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.acerca_de:
                MyLog.i("ActionBar", "Accediendo a acerca de.");
                startActivity(new Intent(ResumenActivity.this, AcercaDeActivity.class));
                return true;
            case R.id.configuracion:
                MyLog.i("ActionBar", "Accediendo a configuracion.");
                return true;
            case R.id.preguntas:
                MyLog.i("ActionBar", "Accediendo a preguntas.");
                startActivity(new Intent(ResumenActivity.this, ListadoPreguntasActivity.class));
            case R.id.salir:
                MyLog.i(TAG, "@string/salir");
                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        MyLog.d(TAG,"Iniciando onStart...");
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyLog.d(TAG,"Iniciando onResume...");
    }

    @Override
    protected void onPause() {
        super.onPause();
        MyLog.d(TAG,"Iniciando onPause...");
    }

    @Override
    protected void onStop() {
        super.onStop();
        MyLog.d(TAG,"Iniciando onStop...");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MyLog.d(TAG,"Iniciando onDestroy...");
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        MyLog.d(TAG,"Iniciando onRestart...");
    }

}
