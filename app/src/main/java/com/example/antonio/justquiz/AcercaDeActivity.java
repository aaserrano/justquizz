package com.example.antonio.justquiz;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class AcercaDeActivity extends AppCompatActivity {


    private String TAG ="AcercaDeActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acerca_de);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbaracerca);

        setSupportActionBar(toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(getResources().getString(R.string.title_activity_acerca_de));
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Asignar la acción necesaria. En este caso "volver atrás"
                    onBackPressed();
                }

            });
        } else {
            MyLog.d("TAG", "Error al cargar toolbar");


        }
    }

    @Override
    public void onBackPressed() {
        // Asignar la acción necesaria. En este caso terminar la actividad
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyLog.d(TAG,"Iniciando onResume...");
    }

    @Override
    protected void onPause() {
        super.onPause();
        MyLog.d(TAG,"Iniciando onPause...");
    }

    @Override
    protected void onStop() {
        super.onStop();
        MyLog.d(TAG,"Iniciando onStop...");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MyLog.d(TAG,"Iniciando onDestroy...");
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        MyLog.d(TAG,"Iniciando onRestart...");
    }


}
